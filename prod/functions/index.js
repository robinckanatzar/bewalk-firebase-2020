const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.deleteOldFCMTokensOnCreate = functions.database.ref('/Team-tokens/{teamId}/{userUID}/fcmToken/{fcmTokenValue}').onCreate((snap, context) => {
  const data = snap.val();
  const getAllTokensPromise = admin.database().ref('/Team-tokens/' + context.params.teamId + '/' + context.params.userUID + '/fcmToken').once('value');

  return getAllTokensPromise.then(result => {
      const tokenSnapShot = result;
      if (!tokenSnapShot.hasChildren()) {
        return consolae.log('There are no other old FCM tokens to delete.');
      }
      console.log('There are', tokenSnapShot.numChildren(), 'old tokens to delete.');
      const tokens = Object.keys(tokenSnapShot.val());

      const tokensToRemove = [];
      for (var i = 0; i < tokenSnapShot.numChildren(); i++) {
          const token = tokens[i];
          console.log('getting promise of user token=',token);
          if(tokens[i] !== context.params.fcmTokenValue) {
            console.log('found token to delete = ' + tokens[i]);
            //AllFollowersFCMPromises[i] = admin.database().ref('/Team-tokens/' + context.params.teamId + '/' + context.params.userUID + '/fcmToken/').once('value');
            tokensToRemove.push(admin.database().ref('/Team-tokens/' + context.params.teamId + '/' + context.params.userUID + '/fcmToken/' + tokens[i]).remove());
          } else {
            console.log('did not find token to delete = ' + tokens[i]);
          }
      }
      return Promise.all(tokensToRemove);
    });
});

// iOS
exports.sendPushNotification = functions.database.ref('/Team-messages/{teamId}/{messageId}').onCreate((snap, context) => {
    const notes = snap.val(); // data
    const createdby = notes.senderId;
    const getAllSubscribersPromise = admin.database().ref('/Team-tokens/' + context.params.teamId + '/subscriptions-ios').once('value');
    console.log('getAllSubscribersPromise: ' + context.getAllSubscribersPromise);

    const payload = {
      notification: {
        title: notes.name,
        body: notes.text
      }
    }

    return getAllSubscribersPromise.then(result => {
        const userUidSnapShot = result;
        if (!userUidSnapShot.hasChildren()) {
          return console.log('There are no subscribed users to write notifications.');
        }
        console.log('There are', userUidSnapShot.numChildren(), 'users to send notifications to.');
        const users = Object.keys(userUidSnapShot.val());

        var AllFollowersFCMPromises = [];
        for (var i = 0; i < userUidSnapShot.numChildren(); i++) {
            const user=users[i];
            console.log('getting promise of user uid=',user);
            if(users[i] === notes.senderId) {
              console.log('found sender uid in subscriptions = ' + users[i]);
              AllFollowersFCMPromises[i] = null;
            } else {
              console.log('did not find sender uid in subscriptions, users[i] = ' + users[i]);
              AllFollowersFCMPromises[i] = admin.database().ref('/Team-tokens/' + context.params.teamId + '/' + users[i] + '/fcmToken/').once('value');
            }
        }

        return Promise.all(AllFollowersFCMPromises).then(results => {

            var tokens = [];
            for(var i in results) {
                var usersTokenSnapShot = results[i];
                console.log('For user = ',i);
                if(usersTokenSnapShot !== null) {
                  if(usersTokenSnapShot.exists()) {
                      if(usersTokenSnapShot.hasChildren()) {
                          const t = Object.keys(usersTokenSnapShot.val());
                          tokens = tokens.concat(t);
                          console.log('token[s] of user = ',t);
                      } else {
                          console.log('else');
                      }
                  }
                }
            }
            console.log('final tokens = ',tokens," notification= ",payload);
            return admin.messaging().sendToDevice(tokens, payload).then(response => {

                const tokensToRemove = [];
                response.results.forEach((result, index) => {
                    const error = result.error;
                    if(error) {
                        console.error('Failure sending notification to uid=', tokens[index], error);
                        if (error.code === 'messaging/invalid-registration-token' || error.code === 'messaging/registration-token-not-registered') {
                            if(usersTokenSnapShot !== null) {
                              tokensToRemove.push(usersTokenSnapShot.child(tokens[index]).remove());
                            }
                        }
                    }
                    else{
                        console.log("notification sent",result);
                    }
                });
                return Promise.all(tokensToRemove);
            });
        });
    });
});

// ANDROID
exports.sendPushNotificationDataOnly = functions.database.ref('/Team-messages/{teamId}/{messageId}').onCreate((snap, context) => {
    console.log('Push notification event triggered 2');

    const notes = snap.val(); // data
    const createdby = notes.senderId;
    const getAllSubscribersPromise = admin.database().ref('/Team-tokens/' + context.params.teamId + '/subscriptions-android').once('value');
    console.log('getAllSubscribersPromise: ' + context.getAllSubscribersPromise);

    const payload = {
      data: {
        title: notes.name,
        body: notes.text
      }
    }

    return getAllSubscribersPromise.then(result => {
        const userUidSnapShot = result;
        if (!userUidSnapShot.hasChildren()) {
          return console.log('There are no subscribed users to write notifications.');
        }
        console.log('There are', userUidSnapShot.numChildren(), 'users to send notifications to.');
        const users = Object.keys(userUidSnapShot.val());

        var AllFollowersFCMPromises = [];
        for (var i = 0; i < userUidSnapShot.numChildren(); i++) {
            const user=users[i];
            console.log('getting promise of user uid=',user);
            if(users[i] === notes.senderId) {
              console.log('found sender uid in subscriptions = ' + users[i]);
              AllFollowersFCMPromises[i] = null;
            } else {
              console.log('did not find sender uid in subscriptions, users[i] = ' + users[i]);
              AllFollowersFCMPromises[i] = admin.database().ref('/Team-tokens/' + context.params.teamId + '/' + users[i] + '/fcmToken/').once('value');
            }
        }

        return Promise.all(AllFollowersFCMPromises).then(results => {

            var tokens = [];
            for(var i in results) {
                var usersTokenSnapShot = results[i];
                console.log('For user = ',i);
                if(usersTokenSnapShot !== null) {
                  if(usersTokenSnapShot.exists()) {
                      if(usersTokenSnapShot.hasChildren()) {
                          const t = Object.keys(usersTokenSnapShot.val());
                          tokens = tokens.concat(t);
                          console.log('token[s] of user = ',t);
                      } else {
                          console.log('else');
                      }
                  }
                }
            }
            console.log('final tokens = ',tokens," notification= ",payload);
            return admin.messaging().sendToDevice(tokens, payload).then(response => {

                const tokensToRemove = [];
                response.results.forEach((result, index) => {
                    const error = result.error;
                    if(error) {
                        console.error('Failure sending notification to uid=', tokens[index], error);
                        if (error.code === 'messaging/invalid-registration-token' || error.code === 'messaging/registration-token-not-registered') {
                            if(usersTokenSnapShot !== null) {
                              tokensToRemove.push(usersTokenSnapShot.child(tokens[index]).remove());
                            }
                        }
                    }
                    else{
                        console.log("notification sent",result);
                    }
                });
                return Promise.all(tokensToRemove);
            });
        });
    });
});
